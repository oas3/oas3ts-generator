import test from "tape-promise/tape.js";
import { injectDeepObjectDictionaryParameterVAlueRequestParameter, injectDeepObjectObjectParameterVAlueRequestParameter } from "./package/src/client-internal.js";
import { extractDeepObjectDictionaryParameterVAlueRequestParameter, extractDeepObjectObjectParameterVAlueRequestParameter } from "./package/src/server-internal.js";

test("inject deep object object", async t => {
    const parameters = {};
    const value = {
        str: ":/?#[]@!$&'()*+,;= ",
        num: 2,
    };
    injectDeepObjectObjectParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue[str]": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
        "V-Alue[num]": "2",
    });
});

test("extract deep object object", async t => {
    const parameters = {
        "V-Alue[str]": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
        "V-Alue[num]": "2",
    };
    const value = extractDeepObjectObjectParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        str: ":/?#[]@!$&'()*+,;= ",
        num: 2,
    });
});

test("inject deep object dictionary", async t => {
    const parameters = {};
    const value = {
        a: ":/?#[]@!$&'()*+,;= ",
        b: "2",
    };
    injectDeepObjectDictionaryParameterVAlueRequestParameter(parameters, value);

    t.deepEqual(parameters, {
        "V-Alue[a]": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
        "V-Alue[b]": "2",
    });
});

test("extract deep object dictionary", async t => {
    const parameters = {
        "V-Alue[a]": "%3A%2F%3F%23%5B%5D%40!%24%26'()*%2B%2C%3B%3D%20",
        "V-Alue[b]": "2",
    };
    const value = extractDeepObjectDictionaryParameterVAlueRequestParameter(parameters);

    t.deepEqual(parameters, {});
    t.deepEqual(value, {
        a: ":/?#[]@!$&'()*+,;= ",
        b: "2",
    });
});
