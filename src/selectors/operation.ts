import { OpenAPIV3 } from "openapi-types";
import { resolveObject, resolvePointerParts } from "../utils/index.js";
import { selectAllPaths } from "./path.js";

export function* selectAllOperations(
    document: OpenAPIV3.Document,
) {
    for (
        const { path, pathObject } of
        selectAllPaths(document)
    ) {
        for (const { operationObject, method } of selectOperations(document, pathObject)) {
            const pathPointerParts = ["paths", path];
            const operationPointerParts = [...pathPointerParts, method];

            const requestBody = resolveObject(document, operationObject.requestBody);
            const requestBodyPointerParts = resolvePointerParts(
                [...operationPointerParts, "requestBody"],
                operationObject.requestBody,
            );

            yield {
                path,
                method,
                pathPointerParts, pathObject,
                operationPointerParts, operationObject,
                requestBodyPointerParts, requestBody,

                pointerParts: operationPointerParts,
            };
        }
    }
}

export function* selectOperations(
    document: OpenAPIV3.Document,
    pathObject: OpenAPIV3.PathItemObject,
) {
    for (const method of Object.values(OpenAPIV3.HttpMethods)) {
        const operationObject = pathObject[method];
        if (!operationObject) continue;

        yield {
            operationObject,
            method,
        };
    }
}

