import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import { resolveObject } from "../utils/index.js";
import { selectAllOperations, selectAllResponses } from "./index.js";

export function* selectAllResponseContentTypes(
    document: OpenAPIV3.Document,
    availableTypes: string[],
) {
    for (
        const { responseObject, responsePointerParts, nameParts } of
        selectAllResponses(document)
    ) {
        yield* selectContentTypesFromContentContainer(
            availableTypes,
            responseObject,
            responsePointerParts,
            nameParts,
        );
    }
}

export function* selectAllRequestContentTypes(
    document: OpenAPIV3.Document,
    availableTypes: string[],
) {
    for (
        const { requestBody, requestBodyPointerParts, operationObject } of
        selectAllOperations(document)
    ) {
        assert(operationObject.operationId != null);

        const nameParts = [operationObject.operationId];
        yield* requestBody != null && requestBodyPointerParts != null ?
            selectContentTypesFromContentContainer(
                availableTypes,
                requestBody,
                requestBodyPointerParts,
                nameParts,
            ) :
            [];
    }
}

export function selectResponseContentTypes(
    document: OpenAPIV3.Document,
    availableTypes: string[],
    operationObject: OpenAPIV3.OperationObject,
) {
    const result = new Set<string>();
    for (
        const [statusKind, maybeResponseObject] of
        Object.entries(operationObject.responses)
    ) {
        const responseObject = resolveObject(document, maybeResponseObject);

        for (const contentType of availableTypes) {
            const mediaTypeObject = responseObject?.content?.[contentType];
            if (!mediaTypeObject) continue;

            result.add(contentType);
        }

    }
    return result;
}

export function selectRequestContentTypes(
    document: OpenAPIV3.Document,
    availableTypes: string[],
    operationObject: OpenAPIV3.OperationObject,
) {
    const result = new Set<string>();
    const requestBodyObject = resolveObject(document, operationObject.requestBody);

    for (const contentType of availableTypes) {
        const mediaTypeObject = requestBodyObject?.content?.[contentType];
        if (!mediaTypeObject) continue;

        result.add(contentType);

    }
    return result;
}

export function* selectContentTypesFromContentContainer(
    availableTypes: string[],
    contentContainer: OpenAPIV3.RequestBodyObject | OpenAPIV3.ResponseObject,
    pointerParts: string[],
    nameParts: string[] = [],
) {
    for (const contentType of availableTypes) {
        const mediaTypeObject = contentContainer.content?.[contentType];
        if (!mediaTypeObject) continue;

        const mediaTypePointerParts = [
            ...pointerParts, "content", contentType,
        ];

        yield {
            contentType,
            mediaTypeObject,
            mediaTypePointerParts,
            pointerParts: mediaTypePointerParts,
            nameParts: [...nameParts, contentType],
        };
    }
}
