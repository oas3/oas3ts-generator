import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllSecuritySchemes } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType, toReference } from "../../utils/index.js";

export function* generateAuthorizationHandlerTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
) {
    for (
        const { securitySchemeObject, securityScheme, pointerParts } of
        selectAllSecuritySchemes(document)
    ) {
        yield createHandlerType(securitySchemeObject, securityScheme, pointerParts);
    }

    function createHandlerType(
        securitySchemeObject: OpenAPIV3.SecuritySchemeObject,
        securityScheme: string,
        pointerParts: string[],
    ) {
        const handlerTypeName = stringifyType([securityScheme, "authorization", "handler"]);
        const authorizationIdentifier = stringifyIdentifier([securityScheme]);
        const securitySchemeTypeReference = toReference(pointerParts);
        const securitySchemeTypeName = nameIndex[securitySchemeTypeReference];

        const functionType = factory.createFunctionTypeNode(
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    factory.createIdentifier("credential"),
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("shared"),
                            securitySchemeTypeName,
                        ),
                        undefined,
                    ),
                    undefined,
                ),
            ],
            factory.createTypeReferenceNode(
                factory.createIdentifier("Promisable"),
                [
                    factory.createUnionTypeNode([
                        factory.createIndexedAccessTypeNode(
                            factory.createTypeReferenceNode(
                                factory.createIdentifier("Authorization"),
                                undefined,
                            ),
                            factory.createLiteralTypeNode(
                                factory.createStringLiteral(authorizationIdentifier),
                            ),
                        ),
                        factory.createKeywordTypeNode(ts.SyntaxKind.VoidKeyword),
                    ]),
                ],
            ),
        );

        return factory.createTypeAliasDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            handlerTypeName,
            [
                factory.createTypeParameterDeclaration(
                    undefined,
                    "Authorization",
                    factory.createTypeReferenceNode("ServerAuthorization"),
                    factory.createTypeReferenceNode("ServerAuthorization"),
                ),
            ],
            functionType,
        );

    }

}
