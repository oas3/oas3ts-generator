import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectAllPaths } from "../../selectors/index.js";
import { stringifyIdentifier } from "../../utils/name.js";
import { generateServeAuthorizeOperationDeclarations, generateServerAuthorizationHandlerDeclarations, generateServerOperationDeclarations, generateServerOperationHandlerDeclarations, generateServerRegisterAuthorizationDeclarations, generateServerRegisterOperationDeclarations } from "../members/index.js";
import { PackageConfig } from "../package.js";

export function createServerClass(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {

    const classDeclaration = createClassDeclaraion();

    return classDeclaration;

    function createClassDeclaraion() {
        const constructorStatements = [...emitConstructorStatements()];

        const constructorDeclaration = factory.createConstructorDeclaration(
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "options",
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            "ServerOptions",
                        ),
                    ),
                ),
            ],
            factory.createBlock(constructorStatements, true),
        );

        const classElements = [
            constructorDeclaration,
            ...generateServerAuthorizationHandlerDeclarations(factory, document),
            ...generateServerOperationHandlerDeclarations(factory, document),
            ...generateServerRegisterAuthorizationDeclarations(factory, document),
            ...generateServerRegisterOperationDeclarations(factory, document),
            ...generateServeAuthorizeOperationDeclarations(factory, document),
            ...generateServerOperationDeclarations(factory, document, config, nameIndex),
        ];

        const classDeclaration = factory.createClassDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            "Server",
            [
                factory.createTypeParameterDeclaration(
                    undefined,
                    "Authorization",
                    factory.createTypeReferenceNode("ServerAuthorization"),
                    factory.createTypeReferenceNode("ServerAuthorization"),
                ),
            ],
            [
                factory.createHeritageClause(
                    ts.SyntaxKind.ExtendsKeyword,
                    [
                        factory.createExpressionWithTypeArguments(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                "ServerBase",
                            ),
                            [],
                        ),
                    ],
                ),
            ],
            classElements,
        );

        return classDeclaration;
    }

    function* emitConstructorStatements() {
        yield factory.createExpressionStatement(factory.createCallExpression(
            factory.createSuper(),
            [],
            [
                factory.createIdentifier("options"),
            ],
        ));

        for (const { path } of selectAllPaths(document)) {
            yield factory.createExpressionStatement(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createPropertyAccessExpression(
                        factory.createThis(),
                        factory.createIdentifier("router"),
                    ),
                    factory.createIdentifier("insertRoute"),
                ),
                undefined,
                [
                    factory.createStringLiteral(path),
                    factory.createStringLiteral(path),
                ],
            ));
        }

        for (const { operationObject, path, method } of selectAllOperations(document)) {
            assert(operationObject.operationId);

            const operationHandlerFn = stringifyIdentifier([operationObject.operationId]);

            yield factory.createExpressionStatement(factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createThis(),
                    factory.createIdentifier("registerOperation"),
                ),
                undefined,
                [
                    factory.createStringLiteral(path),
                    factory.createStringLiteral(method.toUpperCase()),
                    factory.createPropertyAccessExpression(
                        factory.createThis(),
                        factory.createIdentifier(operationHandlerFn),
                    ),
                ],
            ));

        }
    }

}
