import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectParametersFromContainer } from "../../selectors/index.js";
import { stringifyIdentifier, stringifyType, toReference } from "../../utils/index.js";

export function* generateRequestParametersTypeDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
) {
    for (
        const { pathObject, operationObject, path, method } of
        selectAllOperations(document)
    ) {
        yield* emitFromOperation(
            pathObject,
            operationObject,
            path,
            method,
        );
    }

    function* emitFromOperation(
        pathObject: OpenAPIV3.PathItemObject,
        operationObject: OpenAPIV3.OperationObject,
        path: string,
        method: OpenAPIV3.HttpMethods,
    ) {
        assert(operationObject.operationId);

        const typeName = stringifyType([operationObject.operationId, "request", "parameters"]);
        const members = [
            ...emitMembersFromParametersContainer(
                pathObject,
                ["paths", path],
            ),
            ...emitMembersFromParametersContainer(
                operationObject,
                ["paths", path, method],
            ),
        ];

        yield factory.createInterfaceDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            typeName,
            undefined,
            undefined,
            members,
        );
    }

    function* emitMembersFromParametersContainer(
        parametersContainer: OpenAPIV3.PathItemObject | OpenAPIV3.OperationObject,
        pointerParts: string[],
    ) {
        for (
            const { parameterObject, schemaObject, schemaPointerParts } of
            selectParametersFromContainer(document, parametersContainer, pointerParts)
        ) {
            const parameterRequired = parameterObject.required;
            const parameterName = stringifyIdentifier([parameterObject.name]);
            const parameterTypeReference = toReference(schemaPointerParts);
            const parameterTypeName = nameIndex[parameterTypeReference];

            yield factory.createPropertySignature(
                undefined,
                parameterName,
                parameterRequired ?
                    undefined :
                    factory.createToken(ts.SyntaxKind.QuestionToken),
                factory.createTypeReferenceNode(
                    parameterTypeName,
                ),
            );
        }
    }

}
