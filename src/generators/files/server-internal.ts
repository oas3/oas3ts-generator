import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { generateDeserializeEntityDeclarations, generateExtractCredentialsDeclarations, generateExtractParametersDeclarations, generateInjectParametersDeclarations, generateSerializeEntityDeclarations } from "../functions/index.js";
import { PackageConfig } from "../index.js";

export function createServerInternalSourceFile(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
) {
    const nodes = [
        ...emitImports(),
        ...generateExtractCredentialsDeclarations(
            factory,
            document,
            nameIndex,
        ),
        ...generateExtractParametersDeclarations(
            factory,
            document,
            nameIndex,
            "request",
        ),
        ...generateInjectParametersDeclarations(
            factory,
            document,
            nameIndex,
            "response",
        ),
        ...generateDeserializeEntityDeclarations(
            factory,
            document,
            config,
            nameIndex,
            "request",
        ),
        ...generateSerializeEntityDeclarations(
            factory,
            document,
            config,
            nameIndex,
            "response",
        ),
    ];

    const sourceFile = factory.createSourceFile(
        nodes,
        factory.createToken(ts.SyntaxKind.EndOfFileToken),
        ts.NodeFlags.None,
    );

    return sourceFile;

    function* emitImports() {
        yield factory.createImportDeclaration(
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("lib")),
            ),
            factory.createStringLiteral("@oas3/oas3ts-lib"),
        );

        yield factory.createImportDeclaration(
            undefined,
            factory.createImportClause(
                false,
                undefined,
                factory.createNamespaceImport(factory.createIdentifier("shared")),
            ),
            factory.createStringLiteral("./shared.js"),
        );

    }

}

