import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { generateLiteral } from "../../utils/index.js";

export function createSpecificationConstantDeclaration(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
) {
    return factory.createVariableStatement(
        [
            factory.createToken(ts.SyntaxKind.ExportKeyword),
        ],
        factory.createVariableDeclarationList([
            factory.createVariableDeclaration(
                factory.createIdentifier("specification"),
                undefined,
                undefined,
                generateLiteral(factory, document),
            ),
        ], ts.NodeFlags.Const),
    );
}
