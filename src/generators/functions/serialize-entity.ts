import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectAllResponses, selectContentTypesFromContentContainer } from "../../selectors/index.js";
import { getContentKind, resolvePointerParts, stringifyIdentifier, stringifyType, toReference } from "../../utils/index.js";
import { PackageConfig } from "../index.js";

export function* generateSerializeEntityDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
    type: "request" | "response",
) {
    if (type === "request") {
        for (
            const { requestBody, requestBodyPointerParts } of
            selectAllOperations(document)
        ) {
            for (
                const {
                    contentType,
                    mediaTypeObject,
                    mediaTypePointerParts,
                } of requestBody != null && requestBodyPointerParts != null ?
                    selectContentTypesFromContentContainer(
                        config.requestTypes,
                        requestBody,
                        requestBodyPointerParts,
                    ) :
                    []
            ) {
                yield createFunctionDeclaration(
                    mediaTypeObject,
                    mediaTypePointerParts,
                    contentType,
                );
            }
        }
    }
    if (type === "response") {
        for (
            const { responseObject, responsePointerParts } of
            selectAllResponses(document)
        ) {
            for (
                const {
                    contentType,
                    mediaTypeObject,
                    mediaTypePointerParts,
                } of selectContentTypesFromContentContainer(
                    config.responseTypes,
                    responseObject,
                    responsePointerParts,
                )
            ) {
                yield createFunctionDeclaration(
                    mediaTypeObject,
                    mediaTypePointerParts,
                    contentType,
                );
            }

        }
    }

    function createFunctionDeclaration(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
        contentType: string,
    ) {
        const mediaTypeReference = toReference(mediaTypePointerParts);
        const mediaTypeName = nameIndex[mediaTypeReference];

        const functionName = stringifyIdentifier([
            "serialize",
            mediaTypeName,
        ]);

        const statements = [...emitStatements(
            mediaTypeObject,
            mediaTypePointerParts,
            contentType,
        )];

        const parameterType = createParameterTypeNode(
            mediaTypeObject,
            mediaTypePointerParts,
            contentType,
        );
        const returnType = createReturnTypeNode();

        return factory.createFunctionDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            undefined,
            functionName,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "container",
                    undefined,
                    parameterType,
                ),
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "validate",
                    undefined,
                    factory.createKeywordTypeNode(ts.SyntaxKind.BooleanKeyword),
                ),
            ],
            returnType,
            factory.createBlock(statements, true),
        );

    }

    function createParameterTypeNode(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
        contentType: string,
    ) {
        const contentKind = getContentKind(contentType);

        const schemaPointerParts = resolvePointerParts(
            [...mediaTypePointerParts, "schema"],
            mediaTypeObject.schema,
        );
        const schemaReference = schemaPointerParts && toReference(schemaPointerParts);
        const modelTypeName = schemaReference && nameIndex[schemaReference];

        switch (contentKind) {
            case "empty":
                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("OutgoingEmptyContainer"),
                    ),
                );

            case "stream":
                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("OutgoingStreamContainer"),
                    ),
                );

            case "text":
                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("OutgoingTextContainer"),
                    ),
                );

            case "form":
                assert(modelTypeName != null);

                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("OutgoingFormContainer"),
                    ),
                    [factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("shared"),
                            factory.createIdentifier(modelTypeName),
                        ),
                    )],
                );

            case "json":
                assert(modelTypeName != null);

                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("OutgoingJsonContainer"),
                    ),
                    [factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("shared"),
                            factory.createIdentifier(modelTypeName),
                        ),
                    )],
                );

            default: throw new TypeError("unsupported content kind");
        }
    }

    function createReturnTypeNode() {
        return factory.createFunctionTypeNode(
            undefined,
            [factory.createParameterDeclaration(
                undefined,
                undefined,
                factory.createIdentifier("signal"),
                factory.createToken(ts.SyntaxKind.QuestionToken),
                factory.createTypeReferenceNode(factory.createIdentifier("AbortSignal")),
            )],
            factory.createTypeReferenceNode(
                factory.createIdentifier("AsyncIterable"),
                [factory.createTypeReferenceNode(factory.createIdentifier("Uint8Array"))],
            ),
        );
    }

    function emitStatements(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
        contentType: string,
    ): Iterable<ts.Statement> {
        const contentKind = getContentKind(contentType);

        switch (contentKind) {
            case "stream":
                return emitStatementsStream();

            case "text":
                return emitStatementsText();

            case "form":
                return emitStatementsForm(
                    mediaTypeObject,
                    mediaTypePointerParts,
                );

            case "json":
                return emitStatementsJson(
                    mediaTypeObject,
                    mediaTypePointerParts,
                );

            default: throw new TypeError("unsupported content kind");
        }
    }

    function* emitStatementsStream(): Iterable<ts.Statement> {
        yield factory.createReturnStatement(factory.createPropertyAccessExpression(
            factory.createIdentifier("container"),
            factory.createIdentifier("stream"),
        ));
    }

    function* emitStatementsText(): Iterable<ts.Statement> {
        const serializationErrorName = stringifyType(["outgoing", type, "entity", "serialization", "error"]);

        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createStringLiteral("value"),
                factory.createToken(ts.SyntaxKind.InKeyword),
                factory.createIdentifier("container"),
            ),
            factory.createBlock([
                factory.createReturnStatement(factory.createArrowFunction(
                    undefined,
                    undefined,
                    [],
                    undefined,
                    factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("serializeTextValue"),
                        ),
                        undefined,
                        [factory.createCallExpression(
                            factory.createParenthesizedExpression(factory.createFunctionExpression(
                                [factory.createModifier(ts.SyntaxKind.AsyncKeyword)],
                                undefined,
                                undefined,
                                undefined,
                                [],
                                undefined,
                                factory.createBlock([wrapTryCatchStatements([
                                    factory.createVariableStatement(
                                        undefined,
                                        factory.createVariableDeclarationList([
                                            factory.createVariableDeclaration(
                                                factory.createIdentifier("value"),
                                                undefined,
                                                undefined,
                                                factory.createAwaitExpression(
                                                    factory.createCallExpression(
                                                        factory.createPropertyAccessExpression(
                                                            factory.createIdentifier("container"),
                                                            factory.createIdentifier("value"),
                                                        ),
                                                        undefined,
                                                        [],
                                                    ),
                                                ),
                                            ),
                                        ], ts.NodeFlags.Const),
                                    ),
                                    factory.createReturnStatement(factory.createIdentifier("value")),
                                ], serializationErrorName)], true),
                            )),
                            undefined,
                            [],
                        )],
                    ),
                )),
            ], true),
        );
        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createStringLiteral("lines"),
                factory.createToken(ts.SyntaxKind.InKeyword),
                factory.createIdentifier("container"),
            ),
            factory.createBlock([
                factory.createReturnStatement(factory.createArrowFunction(
                    undefined,
                    undefined,
                    [factory.createParameterDeclaration(
                        undefined,
                        undefined,
                        factory.createIdentifier("signal"),
                        factory.createToken(ts.SyntaxKind.QuestionToken),
                        factory.createTypeReferenceNode(
                            factory.createIdentifier("AbortSignal"),
                            undefined,
                        ),
                        undefined,
                    )],
                    undefined,
                    factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("serializeTextLines"),
                        ),
                        undefined,
                        [factory.createCallExpression(
                            factory.createParenthesizedExpression(factory.createFunctionExpression(
                                [factory.createModifier(ts.SyntaxKind.AsyncKeyword)],
                                factory.createToken(ts.SyntaxKind.AsteriskToken),
                                undefined,
                                undefined,
                                [],
                                undefined,
                                factory.createBlock([wrapTryCatchStatements([
                                    factory.createVariableStatement(
                                        undefined,
                                        factory.createVariableDeclarationList([
                                            factory.createVariableDeclaration(
                                                factory.createIdentifier("lines"),
                                                undefined,
                                                undefined,
                                                factory.createCallExpression(
                                                    factory.createPropertyAccessExpression(
                                                        factory.createIdentifier("container"),
                                                        factory.createIdentifier("lines"),
                                                    ),
                                                    undefined,
                                                    [factory.createIdentifier("signal")],
                                                ),
                                            ),
                                        ], ts.NodeFlags.Const),
                                    ),
                                    factory.createExpressionStatement(factory.createYieldExpression(
                                        factory.createToken(ts.SyntaxKind.AsteriskToken),
                                        factory.createIdentifier("lines"),
                                    )),
                                ], serializationErrorName)], true),
                            )),
                            undefined,
                            [],
                        )],
                    ),
                )),
            ], true),
        );
        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createStringLiteral("stream"),
                factory.createToken(ts.SyntaxKind.InKeyword),
                factory.createIdentifier("container"),
            ),
            factory.createBlock(
                [factory.createReturnStatement(factory.createPropertyAccessExpression(
                    factory.createIdentifier("container"),
                    factory.createIdentifier("stream"),
                ))],
                true,
            ),
            undefined,
        );
        yield factory.createThrowStatement(factory.createNewExpression(
            factory.createIdentifier("TypeError"),
            undefined,
            [factory.createStringLiteral("bad container")],
        ));
    }

    function* emitStatementsForm(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
    ): Iterable<ts.Statement> {
        const validationErrorName = stringifyType(["outgoing", type, "entity", "validation", "error"]);
        const serializationErrorName = stringifyType(["outgoing", type, "entity", "serialization", "error"]);

        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createStringLiteral("entity"),
                factory.createToken(ts.SyntaxKind.InKeyword),
                factory.createIdentifier("container"),
            ),
            factory.createBlock([
                factory.createReturnStatement(factory.createArrowFunction(
                    undefined,
                    undefined,
                    [],
                    undefined,
                    factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("serializeFormEntity"),
                        ),
                        undefined,
                        [factory.createCallExpression(
                            factory.createParenthesizedExpression(factory.createFunctionExpression(
                                [factory.createModifier(ts.SyntaxKind.AsyncKeyword)],
                                undefined,
                                undefined,
                                undefined,
                                [],
                                undefined,
                                factory.createBlock([wrapTryCatchStatements([
                                    factory.createVariableStatement(
                                        undefined,
                                        factory.createVariableDeclarationList([
                                            factory.createVariableDeclaration(
                                                factory.createIdentifier("entity"),
                                                undefined,
                                                undefined,
                                                factory.createAwaitExpression(
                                                    factory.createCallExpression(
                                                        factory.createPropertyAccessExpression(
                                                            factory.createIdentifier("container"),
                                                            factory.createIdentifier("entity"),
                                                        ),
                                                        undefined,
                                                        [],
                                                    ),
                                                ),
                                            ),
                                        ], ts.NodeFlags.Const),
                                    ),
                                    factory.createIfStatement(
                                        factory.createIdentifier("validate"),
                                        factory.createBlock([...emitValidateStatements(
                                            mediaTypeObject,
                                            mediaTypePointerParts,
                                            validationErrorName,
                                        )], true),
                                    ),
                                    factory.createReturnStatement(factory.createIdentifier("entity")),
                                ], serializationErrorName)], true),
                            )),
                            undefined,
                            [],
                        )],
                    ),
                )),
            ], true),
        );
        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createStringLiteral("stream"),
                factory.createToken(ts.SyntaxKind.InKeyword),
                factory.createIdentifier("container"),
            ),
            factory.createBlock(
                [factory.createReturnStatement(factory.createPropertyAccessExpression(
                    factory.createIdentifier("container"),
                    factory.createIdentifier("stream"),
                ))],
                true,
            ),
            undefined,
        );
        yield factory.createThrowStatement(factory.createNewExpression(
            factory.createIdentifier("TypeError"),
            undefined,
            [factory.createStringLiteral("bad container")],
        ));
    }

    function* emitStatementsJson(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
    ): Iterable<ts.Statement> {
        const validationErrorName = stringifyType(["outgoing", type, "entity", "validation", "error"]);
        const serializationErrorName = stringifyType(["outgoing", type, "entity", "serialization", "error"]);

        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createStringLiteral("entity"),
                factory.createToken(ts.SyntaxKind.InKeyword),
                factory.createIdentifier("container"),
            ),
            factory.createBlock([
                factory.createReturnStatement(factory.createArrowFunction(
                    undefined,
                    undefined,
                    [],
                    undefined,
                    factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("serializeJsonEntity"),
                        ),
                        undefined,
                        [factory.createCallExpression(
                            factory.createParenthesizedExpression(factory.createFunctionExpression(
                                [factory.createModifier(ts.SyntaxKind.AsyncKeyword)],
                                undefined,
                                undefined,
                                undefined,
                                [],
                                undefined,
                                factory.createBlock([wrapTryCatchStatements([
                                    factory.createVariableStatement(
                                        undefined,
                                        factory.createVariableDeclarationList([
                                            factory.createVariableDeclaration(
                                                factory.createIdentifier("entity"),
                                                undefined,
                                                undefined,
                                                factory.createAwaitExpression(
                                                    factory.createCallExpression(
                                                        factory.createPropertyAccessExpression(
                                                            factory.createIdentifier("container"),
                                                            factory.createIdentifier("entity"),
                                                        ),
                                                        undefined,
                                                        [],
                                                    ),
                                                ),
                                            ),
                                        ], ts.NodeFlags.Const),
                                    ),
                                    factory.createIfStatement(
                                        factory.createIdentifier("validate"),
                                        factory.createBlock([...emitValidateStatements(
                                            mediaTypeObject,
                                            mediaTypePointerParts,
                                            validationErrorName,
                                        )], true),
                                    ),
                                    factory.createReturnStatement(factory.createIdentifier("entity")),
                                ], serializationErrorName)], true),
                            )),
                            undefined,
                            [],
                        )],
                    ),
                )),
            ], true),
        );
        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createStringLiteral("entities"),
                factory.createToken(ts.SyntaxKind.InKeyword),
                factory.createIdentifier("container"),
            ),
            factory.createBlock([
                factory.createReturnStatement(factory.createArrowFunction(
                    undefined,
                    undefined,
                    [factory.createParameterDeclaration(
                        undefined,
                        undefined,
                        factory.createIdentifier("signal"),
                        factory.createToken(ts.SyntaxKind.QuestionToken),
                        factory.createTypeReferenceNode(
                            factory.createIdentifier("AbortSignal"),
                            undefined,
                        ),
                        undefined,
                    )],
                    undefined,
                    factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("serializeJsonEntities"),
                        ),
                        undefined,
                        [factory.createCallExpression(
                            factory.createParenthesizedExpression(factory.createFunctionExpression(
                                [factory.createModifier(ts.SyntaxKind.AsyncKeyword)],
                                factory.createToken(ts.SyntaxKind.AsteriskToken),
                                undefined,
                                undefined,
                                [],
                                undefined,
                                factory.createBlock([wrapTryCatchStatements([
                                    factory.createForOfStatement(
                                        factory.createToken(ts.SyntaxKind.AwaitKeyword),
                                        factory.createVariableDeclarationList([
                                            factory.createVariableDeclaration(
                                                factory.createIdentifier("entity"),
                                                undefined,
                                                undefined,
                                                undefined,
                                            ),
                                        ], ts.NodeFlags.Const),
                                        factory.createCallExpression(
                                            factory.createPropertyAccessExpression(
                                                factory.createIdentifier("container"),
                                                factory.createIdentifier("entities"),
                                            ),
                                            undefined,
                                            [factory.createIdentifier("signal")],
                                        ),
                                        factory.createBlock([
                                            factory.createIfStatement(
                                                factory.createIdentifier("validate"),
                                                factory.createBlock([...emitValidateStatements(
                                                    mediaTypeObject,
                                                    mediaTypePointerParts,
                                                    validationErrorName,
                                                )], true),
                                            ),
                                            factory.createExpressionStatement(
                                                factory.createYieldExpression(
                                                    undefined,
                                                    factory.createIdentifier("entity"),
                                                ),
                                            ),
                                        ], true),
                                    ),
                                ], serializationErrorName)], true),
                            )),
                            undefined,
                            [],
                        )],
                    ),
                )),
            ], true),
        );

        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createStringLiteral("stream"),
                factory.createToken(ts.SyntaxKind.InKeyword),
                factory.createIdentifier("container"),
            ),
            factory.createBlock(
                [factory.createReturnStatement(factory.createPropertyAccessExpression(
                    factory.createIdentifier("container"),
                    factory.createIdentifier("stream"),
                ))],
                true,
            ),
            undefined,
        );
        yield factory.createThrowStatement(factory.createNewExpression(
            factory.createIdentifier("TypeError"),
            undefined,
            [factory.createStringLiteral("bad container")],
        ));
    }

    function* emitValidateStatements(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
        errorTypeName: string,
    ) {
        const schemaPointerParts = resolvePointerParts(
            [...mediaTypePointerParts, "schema"],
            mediaTypeObject.schema,
        );
        const schemaReference = schemaPointerParts && toReference(schemaPointerParts);
        const modelTypeName = schemaReference && nameIndex[schemaReference];

        assert(modelTypeName != null);

        const validateFn = stringifyIdentifier([
            "validate",
            modelTypeName,
        ]);

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([factory.createVariableDeclaration(
                factory.createIdentifier("invalidPaths"),
                undefined,
                undefined,
                factory.createArrayLiteralExpression([
                    factory.createSpreadElement(factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("shared"),
                            factory.createIdentifier(validateFn),
                        ),
                        undefined,
                        [factory.createIdentifier("entity")],
                    )),
                ], false),
            )], ts.NodeFlags.Const),
        );
        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("invalidPaths"),
                    factory.createIdentifier("length"),
                ),
                factory.createToken(ts.SyntaxKind.GreaterThanToken),
                factory.createNumericLiteral(0),
            ),
            factory.createBlock([
                factory.createThrowStatement(factory.createNewExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier(errorTypeName),
                    ),
                    undefined,
                    [factory.createIdentifier("invalidPaths")],
                )),
            ], true),
        );
    }

    function wrapTryCatchStatements(
        statements: ts.Statement[],
        errorTypeName: string,
    ) {
        return factory.createTryStatement(
            factory.createBlock(statements, true),
            factory.createCatchClause(
                factory.createVariableDeclaration(factory.createIdentifier("error")),
                factory.createBlock([
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("error"),
                            factory.createToken(ts.SyntaxKind.InstanceOfKeyword),
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("SerializationError"),
                            ),
                        ),
                        factory.createBlock([
                            factory.createThrowStatement(factory.createNewExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier(errorTypeName),
                                ),
                                undefined,
                                [],
                            )),
                        ], true),
                    ),
                    factory.createThrowStatement(factory.createIdentifier("error")),
                ], true),
            ),
            undefined,
        );
    }

}
