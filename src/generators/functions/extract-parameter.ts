import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { ParameterIn, selectAllRequestParameters, selectAllResponseParameters, selectParameterProperties, selectPropertiesFromSchema, selectSchemaType } from "../../selectors/index.js";
import { resolveObject, resolvePointerParts, stringifyIdentifier, toReference } from "../../utils/index.js";

export function* generateExtractParametersDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    nameIndex: Record<string, string>,
    type: "request" | "response",
) {
    if (type === "request") for (
        const { parameterObject, nameParts, pointerParts } of
        selectAllRequestParameters(document)
    ) {
        yield createFunctionDeclaration(
            parameterObject.name,
            parameterObject.in as ParameterIn,
            parameterObject,
            pointerParts,
        );
    }

    if (type === "response") for (
        const { header, headerObject, nameParts, pointerParts } of
        selectAllResponseParameters(document)
    ) {
        yield createFunctionDeclaration(
            header,
            "header",
            headerObject,
            pointerParts,
        );
    }

    function createFunctionDeclaration(
        parameterName: string,
        parameterIn: ParameterIn,
        parameterObject: OpenAPIV3.ParameterBaseObject,
        parameterPointerParts: string[],
    ) {
        const parameterTypeReference = toReference(parameterPointerParts);
        const parameterTypeName = nameIndex[parameterTypeReference];
        const functionName = stringifyIdentifier([
            "extract",
            parameterTypeName,
        ]);

        const schemaObject = resolveObject(
            document,
            parameterObject.schema,
        );
        assert(schemaObject);

        const schemaPointerParts = resolvePointerParts(
            [...parameterPointerParts, "schema"],
            parameterObject.schema,
        );
        assert(schemaPointerParts);

        const typeReference = toReference(schemaPointerParts);
        const typeName = nameIndex[typeReference];

        const statements = [...emitStatements(
            parameterName,
            parameterIn,
            parameterObject,
            schemaObject,
            schemaPointerParts,
        )];

        return factory.createFunctionDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            undefined,
            functionName,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "parameters",
                    undefined,
                    factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("Parameters"),
                        ),
                    ),
                ),
            ],
            factory.createUnionTypeNode([
                factory.createKeywordTypeNode(ts.SyntaxKind.UndefinedKeyword),
                factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("shared"),
                        factory.createIdentifier(typeName),
                    ),
                ),
            ]),
            factory.createBlock(statements, true),
        );

    }

    function* emitStatements(
        parameterName: string,
        parameterIn: ParameterIn,
        parameterObject: OpenAPIV3.ParameterBaseObject,
        schemaObject: OpenAPIV3.SchemaObject,
        schemaPointerParts: string[],
    ) {
        const {
            prefix, assignment, glue, deepObject,
        } = selectParameterProperties(parameterIn, parameterObject);

        const schemaType = selectSchemaType(schemaObject);
        switch (schemaType) {
            case "string":
            case "number":
            case "integer":
            case "boolean":
                if (deepObject) throw new Error("unsupported type");

                yield* emitPrimitiveStatements(
                    parameterName,
                    parameterIn,
                    schemaType,
                    prefix,
                );
                break;

            case "array":
                if (deepObject) throw new Error("unsupported type");

                assert("items" in schemaObject);

                yield* emitArrayStatements(
                    parameterName,
                    parameterIn,
                    schemaObject,
                    schemaPointerParts,
                    prefix,
                    glue,
                );
                break;

            case "object":
                if (schemaObject.properties != null) {
                    yield* emitObjectStatements(
                        parameterName,
                        parameterIn,
                        schemaObject,
                        schemaPointerParts,
                        prefix,
                        glue,
                        assignment,
                        deepObject,
                    );
                }
                else if (
                    schemaObject.additionalProperties != null &&
                    schemaObject.additionalProperties !== false
                ) {
                    yield* emitDictionaryStatements(
                        parameterName,
                        parameterIn,
                        schemaObject,
                        schemaPointerParts,
                        prefix,
                        glue,
                        assignment,
                        deepObject,
                    );
                }
                else {
                    throw new Error("not supported");
                }
                break;

            default:
                throw new Error(`unexpected schema type ${schemaObject.type}`);
        }
    }

    function* emitPrimitiveStatements(
        parameterName: string,
        parameterIn: ParameterIn,
        parameterType: "boolean" | "number" | "string" | "integer",
        prefix: string | null,
    ) {
        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("parameterValue"),
                    undefined,
                    undefined,
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("getParameterValue"),
                        ),
                        undefined,
                        [
                            factory.createIdentifier("parameters"),
                            createdEncodeParameterNameExpression(
                                parameterIn,
                                factory.createStringLiteral(parameterName),
                            ),
                        ],
                    ),
                ),
            ], ts.NodeFlags.Const),
        );

        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createIdentifier("parameterValue"),
                factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                factory.createNull(),
            ),
            factory.createReturnStatement(),
        );

        if (prefix == null) {
            yield factory.createExpressionStatement(
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("clearParameter"),
                    ),
                    undefined,
                    [
                        factory.createIdentifier("parameters"),
                        createdEncodeParameterNameExpression(
                            parameterIn,
                            factory.createStringLiteral(parameterName),
                        ),
                    ],
                ),
            );
            yield factory.createReturnStatement(
                createdDecodedParameterValueExpression(
                    parameterIn,
                    parameterType,
                    factory.createIdentifier("parameterValue"),
                ),
            );
        }
        else {
            yield factory.createIfStatement(
                factory.createPrefixUnaryExpression(
                    ts.SyntaxKind.ExclamationToken,
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("parameterValue"),
                            factory.createIdentifier("startsWith"),
                        ),
                        undefined,
                        [factory.createStringLiteral(prefix)],
                    ),
                ),
                factory.createReturnStatement(),
            );
            yield factory.createExpressionStatement(
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("clearParameter"),
                    ),
                    undefined,
                    [
                        factory.createIdentifier("parameters"),
                        createdEncodeParameterNameExpression(
                            parameterIn,
                            factory.createStringLiteral(parameterName),
                        ),
                    ],
                ),
            );
            yield factory.createReturnStatement(
                createdDecodedParameterValueExpression(
                    parameterIn,
                    parameterType,
                    factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("parameterValue"),
                            factory.createIdentifier("substring"),
                        ),
                        undefined,
                        [factory.createNumericLiteral(prefix.length)],
                    ),
                ),
            );
        }
    }

    function* emitArrayStatements(
        parameterName: string,
        parameterIn: ParameterIn,
        schemaObject: OpenAPIV3.ArraySchemaObject,
        schemaPointerParts: string[],
        prefix: string | null,
        glue: string | null,
    ) {
        const itemsSchemaObject = resolveObject(document, schemaObject.items);
        assert(itemsSchemaObject);

        const itemsSchemaPointerParts = resolvePointerParts(
            [...schemaPointerParts, "items"],
            schemaObject.items,
        );
        assert(itemsSchemaPointerParts);

        if (glue == null) {
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterValues"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("getParameterValues"),
                            ),
                            undefined,
                            [
                                factory.createIdentifier("parameters"),
                                createdEncodeParameterNameExpression(
                                    parameterIn,
                                    factory.createStringLiteral(parameterName),
                                ),
                            ],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );
            yield factory.createIfStatement(
                factory.createBinaryExpression(
                    factory.createIdentifier("parameterValues"),
                    factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                    factory.createNull(),
                ),
                factory.createReturnStatement(),
            );
        }
        else {
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterValue"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("getParameterValue"),
                            ),
                            undefined,
                            [
                                factory.createIdentifier("parameters"),
                                createdEncodeParameterNameExpression(
                                    parameterIn,
                                    factory.createStringLiteral(parameterName),
                                ),
                            ],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );
            yield factory.createIfStatement(
                factory.createBinaryExpression(
                    factory.createIdentifier("parameterValue"),
                    factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                    factory.createNull(),
                ),
                factory.createReturnStatement(),
            );
            if (prefix != null) {
                yield factory.createIfStatement(
                    factory.createPrefixUnaryExpression(
                        ts.SyntaxKind.ExclamationToken,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("parameterValue"),
                                factory.createIdentifier("startsWith"),
                            ),
                            undefined,
                            [factory.createStringLiteral(prefix)],
                        ),
                    ),
                    factory.createReturnStatement(),
                );
            }
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterValues"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                prefix == null ?
                                    factory.createIdentifier("parameterValue") :
                                    factory.createCallExpression(
                                        factory.createPropertyAccessChain(
                                            factory.createIdentifier("parameterValue"),
                                            undefined,
                                            "substring",
                                        ),
                                        undefined,
                                        [
                                            factory.createNumericLiteral(prefix.length),
                                        ],
                                    ),
                                factory.createIdentifier("split"),
                            ),
                            undefined,
                            [factory.createStringLiteral(glue)],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );
        }
        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("parameterValues"),
                    factory.createIdentifier("length"),
                ),
                factory.createToken(ts.SyntaxKind.EqualsEqualsEqualsToken),
                factory.createNumericLiteral(0),
            ),
            factory.createReturnStatement(),
        );
        yield factory.createExpressionStatement(
            factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("lib"),
                    factory.createIdentifier("clearParameter"),
                ),
                undefined,
                [
                    factory.createIdentifier("parameters"),
                    createdEncodeParameterNameExpression(
                        parameterIn,
                        factory.createStringLiteral(parameterName),
                    ),
                ],
            ),
        );
        yield factory.createReturnStatement(
            factory.createCallExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("parameterValues"),
                    factory.createIdentifier("map"),
                ),
                undefined,
                [factory.createArrowFunction(
                    undefined,
                    undefined,
                    [factory.createParameterDeclaration(
                        undefined,
                        undefined,
                        factory.createIdentifier("parameterValue"),
                    )],
                    undefined,
                    factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                    createdDecodedParameterValueExpression(
                        parameterIn,
                        selectSchemaType(itemsSchemaObject),
                        factory.createIdentifier("parameterValue"),
                    ),
                )],
            ),
        );
    }

    function* emitObjectStatements(
        parameterName: string,
        parameterIn: ParameterIn,
        schemaObject: OpenAPIV3.SchemaObject,
        schemaPointerParts: string[],
        prefix: string | null,
        glue: string | null,
        assignment: string | null,
        deepObject: boolean,
    ) {
        const typeReference = toReference(schemaPointerParts);
        const typeName = nameIndex[typeReference];

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("value"),
                    undefined,
                    factory.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword),
                    factory.createObjectLiteralExpression([], false),
                ),
            ], ts.NodeFlags.Const),
        );

        if (deepObject) {
            for (const { property, propertySchemaObject } of selectPropertiesFromSchema(
                document, schemaObject, schemaPointerParts,
            )) {
                const propertyType = selectSchemaType(propertySchemaObject);

                yield factory.createBlock([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("parameterName"),
                                undefined,
                                undefined,
                                factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("lib"),
                                        factory.createIdentifier("stringifyDeepObjectPropertyName"),
                                    ),
                                    undefined,
                                    [
                                        factory.createStringLiteral(parameterName),
                                        factory.createStringLiteral(property),
                                    ],
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("parameterValue"),
                                undefined,
                                undefined,
                                factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("lib"),
                                        factory.createIdentifier("getParameterValue"),
                                    ),
                                    undefined,
                                    [
                                        factory.createIdentifier("parameters"),
                                        createdEncodeParameterNameExpression(
                                            parameterIn,
                                            factory.createIdentifier("parameterName"),
                                        ),
                                    ],
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("parameterValue"),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createBlock([
                            factory.createExpressionStatement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("clearParameter"),
                                ),
                                undefined,
                                [
                                    factory.createIdentifier("parameters"),
                                    createdEncodeParameterNameExpression(
                                        parameterIn,
                                        factory.createIdentifier("parameterName"),
                                    ),
                                ],
                            )),
                            factory.createExpressionStatement(factory.createBinaryExpression(
                                factory.createElementAccessExpression(
                                    factory.createIdentifier("value"),
                                    factory.createStringLiteral(property),
                                ),
                                factory.createToken(ts.SyntaxKind.EqualsToken),
                                createdDecodedParameterValueExpression(
                                    parameterIn,
                                    propertyType,
                                    factory.createIdentifier("parameterValue"),
                                ),
                            )),
                        ], true),
                    ),
                ], true);

            }
        }
        else if (glue == null || assignment == null) {
            for (const { property, propertySchemaObject } of selectPropertiesFromSchema(
                document, schemaObject, schemaPointerParts,
            )) {
                const propertyType = selectSchemaType(propertySchemaObject);

                yield factory.createBlock([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("parameterValue"),
                                undefined,
                                undefined,
                                factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("lib"),
                                        factory.createIdentifier("getParameterValue"),
                                    ),
                                    undefined,
                                    [
                                        factory.createIdentifier("parameters"),
                                        createdEncodeParameterNameExpression(
                                            parameterIn,
                                            factory.createStringLiteral(property),
                                        ),
                                    ],
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("parameterValue"),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createBlock([
                            factory.createExpressionStatement(factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("clearParameter"),
                                ),
                                undefined,
                                [
                                    factory.createIdentifier("parameters"),
                                    createdEncodeParameterNameExpression(
                                        parameterIn,
                                        factory.createStringLiteral(property),
                                    ),
                                ],
                            )),
                            factory.createExpressionStatement(factory.createBinaryExpression(
                                factory.createElementAccessExpression(
                                    factory.createIdentifier("value"),
                                    factory.createStringLiteral(property),
                                ),
                                factory.createToken(ts.SyntaxKind.EqualsToken),
                                createdDecodedParameterValueExpression(
                                    parameterIn,
                                    propertyType,
                                    factory.createIdentifier("parameterValue"),
                                ),
                            )),
                        ], true),
                    ),
                ], true);

            }
        }
        else {
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterValue"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("getParameterValue"),
                            ),
                            undefined,
                            [
                                factory.createIdentifier("parameters"),
                                createdEncodeParameterNameExpression(
                                    parameterIn,
                                    factory.createStringLiteral(parameterName),
                                ),
                            ],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );
            yield factory.createIfStatement(
                factory.createBinaryExpression(
                    factory.createIdentifier("parameterValue"),
                    factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                    factory.createNull(),
                ),
                factory.createReturnStatement(),
            );
            if (prefix != null) {
                yield factory.createIfStatement(
                    factory.createPrefixUnaryExpression(
                        ts.SyntaxKind.ExclamationToken,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("parameterValue"),
                                factory.createIdentifier("startsWith"),
                            ),
                            undefined,
                            [factory.createStringLiteral(prefix)],
                        ),
                    ),
                    factory.createReturnStatement(),
                );
            }
            yield factory.createExpressionStatement(
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("clearParameter"),
                    ),
                    undefined,
                    [
                        factory.createIdentifier("parameters"),
                        createdEncodeParameterNameExpression(
                            parameterIn,
                            factory.createStringLiteral(parameterName),
                        ),
                    ],
                ),
            );

            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("encodedValues"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("Object"),
                                factory.createIdentifier("fromEntries"),
                            ),
                            undefined,
                            [factory.createCallExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier("parseParameterEntries"),
                                ),
                                undefined,
                                [
                                    factory.createIdentifier("parameterValue"),
                                    factory.createStringLiteral(prefix ?? ""),
                                    factory.createStringLiteral(glue),
                                    factory.createStringLiteral(assignment),
                                ],
                            )],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );

            for (const { property, propertySchemaObject } of selectPropertiesFromSchema(
                document, schemaObject, schemaPointerParts,
            )) {
                const propertyType = selectSchemaType(propertySchemaObject);

                yield factory.createBlock([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("encodedValue"),
                                undefined,
                                undefined,
                                factory.createElementAccessExpression(
                                    factory.createIdentifier("encodedValues"),
                                    factory.createStringLiteral(property),
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("encodedValue"),
                            factory.createToken(ts.SyntaxKind.ExclamationEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createBlock([
                            factory.createExpressionStatement(factory.createBinaryExpression(
                                factory.createElementAccessExpression(
                                    factory.createIdentifier("value"),
                                    factory.createStringLiteral(property),
                                ),
                                factory.createToken(ts.SyntaxKind.EqualsToken),
                                createdDecodedParameterValueExpression(
                                    parameterIn,
                                    propertyType,
                                    factory.createIdentifier("encodedValue"),
                                ),
                            )),
                        ], true),
                    ),
                ], true);
            }

        }

        yield factory.createReturnStatement(factory.createAsExpression(
            factory.createIdentifier("value"),
            factory.createTypeReferenceNode(
                factory.createQualifiedName(
                    factory.createIdentifier("shared"),
                    factory.createIdentifier(typeName),
                ),
            ),
        ));
    }

    function* emitDictionaryStatements(
        parameterName: string,
        parameterIn: ParameterIn,
        schemaObject: OpenAPIV3.SchemaObject,
        schemaPointerParts: string[],
        prefix: string | null,
        glue: string | null,
        assignment: string | null,
        deepObject: boolean,
    ) {
        assert(
            schemaObject.additionalProperties != null &&
            schemaObject.additionalProperties !== false,
        );
        assert(schemaObject.additionalProperties !== true);

        const typeReference = toReference(schemaPointerParts);
        const typeName = nameIndex[typeReference];

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([
                factory.createVariableDeclaration(
                    factory.createIdentifier("value"),
                    undefined,
                    factory.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword),
                    factory.createObjectLiteralExpression([], false),
                ),
            ], ts.NodeFlags.Const),
        );

        const additionalPropertiesSchemaObject = resolveObject(
            document,
            schemaObject.additionalProperties,
        );
        assert(additionalPropertiesSchemaObject);

        const additionalPropertiesSchemaObjectPointerParts = resolvePointerParts(
            [...schemaPointerParts, "additionalProperties"],
            schemaObject.additionalProperties,
        );
        assert(additionalPropertiesSchemaObjectPointerParts);

        const additionalPropertiesType = selectSchemaType(additionalPropertiesSchemaObject);

        if (deepObject) {
            yield factory.createForOfStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterName"),
                    ),
                ], ts.NodeFlags.Const),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("getAllParameterNames"),
                    ),
                    undefined,
                    [factory.createIdentifier("parameters")],
                ),
                factory.createBlock([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("propertyName"),
                                undefined,
                                undefined,
                                factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("lib"),
                                        factory.createIdentifier("parseDeepObjectPropertyName"),
                                    ),
                                    undefined,
                                    [
                                        factory.createIdentifier("parameterName"),
                                        factory.createStringLiteral(parameterName),
                                    ],
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("propertyName"),
                            factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createContinueStatement(),
                    ),
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("parameterValue"),
                                undefined,
                                undefined,
                                factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("lib"),
                                        factory.createIdentifier("getParameterValue"),
                                    ),
                                    undefined,
                                    [
                                        factory.createIdentifier("parameters"),
                                        createdEncodeParameterNameExpression(
                                            parameterIn,
                                            factory.createIdentifier("parameterName"),
                                        ),
                                    ],
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("parameterValue"),
                            factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createContinueStatement(),
                    ),
                    factory.createExpressionStatement(factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("clearParameter"),
                        ),
                        undefined,
                        [
                            factory.createIdentifier("parameters"),
                            createdEncodeParameterNameExpression(
                                parameterIn,
                                factory.createIdentifier("parameterName"),
                            ),
                        ],
                    )),
                    factory.createExpressionStatement(factory.createBinaryExpression(
                        factory.createElementAccessExpression(
                            factory.createIdentifier("value"),
                            factory.createIdentifier("propertyName"),
                        ),
                        factory.createToken(ts.SyntaxKind.EqualsToken),
                        createdDecodedParameterValueExpression(
                            parameterIn,
                            additionalPropertiesType,
                            factory.createIdentifier("parameterValue"),
                        ),
                    )),
                ], true),
            );
        }
        else if (glue == null || assignment == null) {
            yield factory.createForOfStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterName"),
                    ),
                ], ts.NodeFlags.Const),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("getAllParameterNames"),
                    ),
                    undefined,
                    [factory.createIdentifier("parameters")],
                ),
                factory.createBlock([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("parameterValue"),
                                undefined,
                                undefined,
                                factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("lib"),
                                        factory.createIdentifier("getParameterValue"),
                                    ),
                                    undefined,
                                    [
                                        factory.createIdentifier("parameters"),
                                        createdEncodeParameterNameExpression(
                                            parameterIn,
                                            factory.createIdentifier("parameterName"),
                                        ),
                                    ],
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("parameterValue"),
                            factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                            factory.createNull(),
                        ),
                        factory.createContinueStatement(),
                    ),
                    factory.createExpressionStatement(factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("lib"),
                            factory.createIdentifier("clearParameter"),
                        ),
                        undefined,
                        [
                            factory.createIdentifier("parameters"),
                            createdEncodeParameterNameExpression(
                                parameterIn,
                                factory.createIdentifier("parameterName"),
                            ),
                        ],
                    )),
                    factory.createExpressionStatement(factory.createBinaryExpression(
                        factory.createElementAccessExpression(
                            factory.createIdentifier("value"),
                            factory.createIdentifier("parameterName"),
                        ),
                        factory.createToken(ts.SyntaxKind.EqualsToken),
                        createdDecodedParameterValueExpression(
                            parameterIn,
                            additionalPropertiesType,
                            factory.createIdentifier("parameterValue"),
                        ),
                    )),
                ], true),
            );
        }
        else {
            yield factory.createVariableStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createIdentifier("parameterValue"),
                        undefined,
                        undefined,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("getParameterValue"),
                            ),
                            undefined,
                            [
                                factory.createIdentifier("parameters"),
                                createdEncodeParameterNameExpression(
                                    parameterIn,
                                    factory.createStringLiteral(parameterName),
                                ),
                            ],
                        ),
                    ),
                ], ts.NodeFlags.Const),
            );
            yield factory.createIfStatement(
                factory.createBinaryExpression(
                    factory.createIdentifier("parameterValue"),
                    factory.createToken(ts.SyntaxKind.EqualsEqualsToken),
                    factory.createNull(),
                ),
                factory.createReturnStatement(),
            );
            if (prefix != null) {
                yield factory.createIfStatement(
                    factory.createPrefixUnaryExpression(
                        ts.SyntaxKind.ExclamationToken,
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("parameterValue"),
                                factory.createIdentifier("startsWith"),
                            ),
                            undefined,
                            [factory.createStringLiteral(prefix)],
                        ),
                    ),
                    factory.createReturnStatement(),
                );
            }
            yield factory.createExpressionStatement(
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("clearParameter"),
                    ),
                    undefined,
                    [
                        factory.createIdentifier("parameters"),
                        createdEncodeParameterNameExpression(
                            parameterIn,
                            factory.createStringLiteral(parameterName),
                        ),
                    ],
                ),
            );

            yield factory.createForOfStatement(
                undefined,
                factory.createVariableDeclarationList([
                    factory.createVariableDeclaration(
                        factory.createArrayBindingPattern([
                            factory.createBindingElement(
                                undefined,
                                undefined,
                                factory.createIdentifier("propertyName"),
                            ),
                            factory.createBindingElement(
                                undefined,
                                undefined,
                                factory.createIdentifier("encodedValue"),
                            ),
                        ])),
                ], ts.NodeFlags.Const),
                factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("parseParameterEntries"),
                    ),
                    undefined,
                    [
                        factory.createIdentifier("parameterValue"),
                        factory.createStringLiteral(prefix ?? ""),
                        factory.createStringLiteral(glue),
                        factory.createStringLiteral(assignment),
                    ],
                ),
                factory.createBlock([
                    factory.createExpressionStatement(factory.createBinaryExpression(
                        factory.createElementAccessExpression(
                            factory.createIdentifier("value"),
                            factory.createIdentifier("propertyName"),
                        ),
                        factory.createToken(ts.SyntaxKind.EqualsToken),
                        createdDecodedParameterValueExpression(
                            parameterIn,
                            additionalPropertiesType,
                            factory.createIdentifier("encodedValue"),
                        ),
                    )),
                ], true),
            );
        }

        yield factory.createReturnStatement(factory.createAsExpression(
            factory.createIdentifier("value"),
            factory.createTypeReferenceNode(
                factory.createQualifiedName(
                    factory.createIdentifier("shared"),
                    factory.createIdentifier(typeName),
                ),
            ),
        ));
    }

    function createdDecodedParameterValueExpression(
        parameterIn: ParameterIn,
        type: "boolean" | "object" | "number" | "string" | "integer" | "array" | undefined,
        parameterValueExpression: ts.Expression,
    ): ts.Expression {
        switch (type) {
            case "string":
                switch (parameterIn) {
                    case "path":
                    case "query": return factory.createCallExpression(
                        factory.createIdentifier("decodeURIComponent"),
                        undefined,
                        [
                            parameterValueExpression,
                        ],
                    );

                    default:
                        return parameterValueExpression;
                }

            case "number":
            case "integer":
                return factory.createCallExpression(
                    factory.createIdentifier("Number"),
                    undefined,
                    [
                        parameterValueExpression,
                    ],
                );

            case "boolean":
                return factory.createCallExpression(
                    factory.createIdentifier("Boolean"),
                    undefined,
                    [
                        parameterValueExpression,
                    ],
                );

            default:
                throw new Error(`unexpected schema type ${type}`);

        }
    }

    function createdEncodeParameterNameExpression(
        parameterIn: ParameterIn,
        parameterNameExpression: ts.Expression,
    ) {
        switch (parameterIn) {
            case "header":
                return factory.createCallExpression(
                    factory.createPropertyAccessExpression(
                        parameterNameExpression,
                        "toLowerCase",
                    ),
                    undefined,
                    [],
                );

            default: return parameterNameExpression;
        }
    }

}
