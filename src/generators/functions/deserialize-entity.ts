import assert from "assert";
import { OpenAPIV3 } from "openapi-types";
import ts from "typescript";
import { selectAllOperations, selectAllResponses, selectContentTypesFromContentContainer } from "../../selectors/index.js";
import { getContentKind, resolvePointerParts, stringifyIdentifier, stringifyType, toReference } from "../../utils/index.js";
import { PackageConfig } from "../index.js";

export function* generateDeserializeEntityDeclarations(
    factory: ts.NodeFactory,
    document: OpenAPIV3.Document,
    config: PackageConfig,
    nameIndex: Record<string, string>,
    type: "request" | "response",
) {
    if (type === "request") {
        for (
            const { requestBody, requestBodyPointerParts } of
            selectAllOperations(document)
        ) {

            for (
                const {
                    contentType,
                    mediaTypeObject,
                    mediaTypePointerParts,
                } of
                requestBody != null && requestBodyPointerParts != null ?
                    selectContentTypesFromContentContainer(
                        config.requestTypes,
                        requestBody,
                        requestBodyPointerParts,
                    ) :
                    []
            ) {
                yield createFunctionDeclaration(
                    mediaTypeObject,
                    mediaTypePointerParts,
                    contentType,
                );
            }
        }
    }

    if (type === "response") {
        for (
            const { responseObject, responsePointerParts } of
            selectAllResponses(document)
        ) {
            for (
                const {
                    contentType,
                    mediaTypeObject,
                    mediaTypePointerParts,
                } of selectContentTypesFromContentContainer(
                    config.responseTypes,
                    responseObject,
                    responsePointerParts,
                )
            ) {
                yield createFunctionDeclaration(
                    mediaTypeObject,
                    mediaTypePointerParts,
                    contentType,
                );
            }

        }

    }

    function createFunctionDeclaration(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
        contentType: string,
    ) {
        const mediaTypeReference = toReference(mediaTypePointerParts);
        const mediaTypeName = nameIndex[mediaTypeReference];

        const functionName = stringifyIdentifier([
            "deserialize",
            mediaTypeName,
        ]);

        const statements = [...emitStatements(
            mediaTypeObject,
            mediaTypePointerParts,
            contentType,
        )];

        const parameterType = createParameterTypeNode();
        const returnType = createReturnTypeNode(
            mediaTypeObject,
            mediaTypePointerParts,
            contentType,
        );

        return factory.createFunctionDeclaration(
            [
                factory.createToken(ts.SyntaxKind.ExportKeyword),
            ],
            undefined,
            functionName,
            undefined,
            [
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "stream",
                    undefined,
                    parameterType,
                ),
                factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    "validate",
                    undefined,
                    factory.createKeywordTypeNode(ts.SyntaxKind.BooleanKeyword),
                ),
            ],
            returnType,
            factory.createBlock(statements, true),
        );

    }

    function createParameterTypeNode() {
        return factory.createFunctionTypeNode(
            undefined,
            [factory.createParameterDeclaration(
                undefined,
                undefined,
                factory.createIdentifier("signal"),
                factory.createToken(ts.SyntaxKind.QuestionToken),
                factory.createTypeReferenceNode(factory.createIdentifier("AbortSignal")),
            )],
            factory.createTypeReferenceNode(
                factory.createIdentifier("AsyncIterable"),
                [factory.createTypeReferenceNode(factory.createIdentifier("Uint8Array"))],
            ),
        );
    }

    function createReturnTypeNode(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
        contentType: string,
    ) {
        const contentKind = getContentKind(contentType);

        const schemaPointerParts = resolvePointerParts(
            [...mediaTypePointerParts, "schema"],
            mediaTypeObject.schema,
        );
        const schemaReference = schemaPointerParts && toReference(schemaPointerParts);
        const modelTypeName = schemaReference && nameIndex[schemaReference];

        switch (contentKind) {
            case "empty":
                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("IncomingEmptyContainer"),
                    ),
                );

            case "stream":
                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("IncomingStreamContainer"),
                    ),
                );

            case "text":
                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("IncomingTextContainer"),
                    ),
                );

            case "form":
                assert(modelTypeName != null);

                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("IncomingFormContainer"),
                    ),
                    [factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("shared"),
                            factory.createIdentifier(modelTypeName),
                        ),
                        undefined,
                    )],
                );

            case "json":
                assert(modelTypeName != null);

                return factory.createTypeReferenceNode(
                    factory.createQualifiedName(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier("IncomingJsonContainer"),
                    ),
                    [factory.createTypeReferenceNode(
                        factory.createQualifiedName(
                            factory.createIdentifier("shared"),
                            factory.createIdentifier(modelTypeName),
                        ),
                        undefined,
                    )],
                );

            default: throw new TypeError("unsupported content kind");
        }

    }

    function emitStatements(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
        contentType: string,
    ): Iterable<ts.Statement> {
        const contentKind = getContentKind(contentType);

        switch (contentKind) {
            case "stream":
                return emitStatementsStream();

            case "text":
                return emitStatementsText();

            case "form":
                return emitStatementsForm(
                    mediaTypeObject,
                    mediaTypePointerParts,
                );

            case "json":
                return emitStatementsJson(
                    mediaTypeObject,
                    mediaTypePointerParts,
                );

            default: throw new TypeError("unsupported content kind");
        }
    }

    function* emitStatementsStream(): Iterable<ts.Statement> {
        yield factory.createReturnStatement(factory.createObjectLiteralExpression(
            [factory.createShorthandPropertyAssignment(
                factory.createIdentifier("stream"),
                undefined,
            )],
            false,
        ));
    }

    function* emitStatementsText(): Iterable<ts.Statement> {
        const deserializationErrorName = stringifyType(["incoming", type, "entity", "deserialization", "error"]);

        yield factory.createReturnStatement(factory.createObjectLiteralExpression([
            factory.createShorthandPropertyAssignment(
                factory.createIdentifier("stream"),
                undefined,
            ),
            factory.createMethodDeclaration(
                [factory.createModifier(ts.SyntaxKind.AsyncKeyword)],
                undefined,
                factory.createIdentifier("value"),
                undefined,
                undefined,
                [],
                undefined,
                factory.createBlock([wrapTryCatchStatements([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("value"),
                                undefined,
                                undefined,
                                factory.createAwaitExpression(factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("lib"),
                                        factory.createIdentifier("deserializeTextValue"),
                                    ),
                                    undefined,
                                    [factory.createIdentifier("stream")],
                                )),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createReturnStatement(factory.createIdentifier("value")),
                ], deserializationErrorName)], true),
            ),
            factory.createMethodDeclaration(
                [factory.createModifier(ts.SyntaxKind.AsyncKeyword)],
                factory.createToken(ts.SyntaxKind.AsteriskToken),
                factory.createIdentifier("lines"),
                undefined,
                undefined,
                [factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    factory.createIdentifier("signal"),
                    factory.createToken(ts.SyntaxKind.QuestionToken),
                    factory.createTypeReferenceNode(
                        factory.createIdentifier("AbortSignal"),
                    ),
                )],
                undefined,
                factory.createBlock([wrapTryCatchStatements([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("lines"),
                                undefined,
                                undefined,
                                factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("lib"),
                                        factory.createIdentifier("deserializeTextLines"),
                                    ),
                                    undefined,
                                    [
                                        factory.createIdentifier("stream"),
                                        factory.createIdentifier("signal"),
                                    ],
                                ),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createExpressionStatement(factory.createYieldExpression(
                        factory.createToken(ts.SyntaxKind.AsteriskToken),
                        factory.createIdentifier("lines"),
                    )),
                ], deserializationErrorName)], true),
            ),
        ], true));
    }

    function* emitStatementsForm(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
    ): Iterable<ts.Statement> {
        const schemaPointerParts = resolvePointerParts(
            [...mediaTypePointerParts, "schema"],
            mediaTypeObject.schema,
        );
        const schemaReference = schemaPointerParts && toReference(schemaPointerParts);
        const modelTypeName = schemaReference && nameIndex[schemaReference];

        assert(modelTypeName != null);

        const validationErrorName = stringifyType(["incoming", type, "entity", "validation", "error"]);
        const deserializationErrorName = stringifyType(["incoming", type, "entity", "deserialization", "error"]);

        yield factory.createReturnStatement(factory.createObjectLiteralExpression([
            factory.createShorthandPropertyAssignment(
                factory.createIdentifier("stream"),
                undefined,
            ),
            factory.createMethodDeclaration(
                [factory.createModifier(ts.SyntaxKind.AsyncKeyword)],
                undefined,
                factory.createIdentifier("entity"),
                undefined,
                undefined,
                [],
                undefined,
                factory.createBlock([wrapTryCatchStatements([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("entity"),
                                undefined,
                                undefined,
                                factory.createAwaitExpression(factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("lib"),
                                        factory.createIdentifier("deserializeFormEntity"),
                                    ),
                                    [factory.createTypeReferenceNode(
                                        factory.createQualifiedName(
                                            factory.createIdentifier("shared"),
                                            factory.createIdentifier(modelTypeName),
                                        ),
                                    )],
                                    [factory.createIdentifier("stream")],
                                )),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createIdentifier("validate"),
                        factory.createBlock([...emitValidateStatements(
                            mediaTypeObject,
                            mediaTypePointerParts,
                            validationErrorName,
                        )], true),
                    ),
                    factory.createReturnStatement(factory.createIdentifier("entity")),
                ], deserializationErrorName)], true),
            ),
        ], true));
    }

    function* emitStatementsJson(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
    ): Iterable<ts.Statement> {
        const schemaPointerParts = resolvePointerParts(
            [...mediaTypePointerParts, "schema"],
            mediaTypeObject.schema,
        );
        const schemaReference = schemaPointerParts && toReference(schemaPointerParts);
        const modelTypeName = schemaReference && nameIndex[schemaReference];

        assert(modelTypeName != null);

        const validationErrorName = stringifyType(["incoming", type, "entity", "validation", "error"]);
        const deserializationErrorName = stringifyType(["incoming", type, "entity", "deserialization", "error"]);

        yield factory.createReturnStatement(factory.createObjectLiteralExpression([
            factory.createShorthandPropertyAssignment(
                factory.createIdentifier("stream"),
                undefined,
            ),
            factory.createMethodDeclaration(
                [factory.createModifier(ts.SyntaxKind.AsyncKeyword)],
                undefined,
                factory.createIdentifier("entity"),
                undefined,
                undefined,
                [],
                undefined,
                factory.createBlock([wrapTryCatchStatements([
                    factory.createVariableStatement(
                        undefined,
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("entity"),
                                undefined,
                                undefined,
                                factory.createAwaitExpression(factory.createCallExpression(
                                    factory.createPropertyAccessExpression(
                                        factory.createIdentifier("lib"),
                                        factory.createIdentifier("deserializeJsonEntity"),
                                    ),
                                    [factory.createTypeReferenceNode(
                                        factory.createQualifiedName(
                                            factory.createIdentifier("shared"),
                                            factory.createIdentifier(modelTypeName),
                                        ),
                                    )],
                                    [factory.createIdentifier("stream")],
                                )),
                            ),
                        ], ts.NodeFlags.Const),
                    ),
                    factory.createIfStatement(
                        factory.createIdentifier("validate"),
                        factory.createBlock([...emitValidateStatements(
                            mediaTypeObject,
                            mediaTypePointerParts,
                            validationErrorName,
                        )], true),
                    ),
                    factory.createReturnStatement(factory.createIdentifier("entity")),
                ], deserializationErrorName)], true),
            ),
            factory.createMethodDeclaration(
                [factory.createModifier(ts.SyntaxKind.AsyncKeyword)],
                factory.createToken(ts.SyntaxKind.AsteriskToken),
                factory.createIdentifier("entities"),
                undefined,
                undefined,
                [factory.createParameterDeclaration(
                    undefined,
                    undefined,
                    factory.createIdentifier("signal"),
                    factory.createToken(ts.SyntaxKind.QuestionToken),
                    factory.createTypeReferenceNode(
                        factory.createIdentifier("AbortSignal"),
                    ),
                )],
                undefined,
                factory.createBlock([wrapTryCatchStatements([
                    factory.createForOfStatement(
                        factory.createToken(ts.SyntaxKind.AwaitKeyword),
                        factory.createVariableDeclarationList([
                            factory.createVariableDeclaration(
                                factory.createIdentifier("entity"),
                            ),
                        ], ts.NodeFlags.Const),
                        factory.createCallExpression(
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("deserializeJsonEntities"),
                            ),
                            [factory.createTypeReferenceNode(
                                factory.createQualifiedName(
                                    factory.createIdentifier("shared"),
                                    factory.createIdentifier(modelTypeName),
                                ),
                            )],
                            [
                                factory.createIdentifier("stream"),
                                factory.createIdentifier("signal"),
                            ],
                        ),
                        factory.createBlock([
                            factory.createIfStatement(
                                factory.createIdentifier("validate"),
                                factory.createBlock([...emitValidateStatements(
                                    mediaTypeObject,
                                    mediaTypePointerParts,
                                    validationErrorName,
                                )], true),
                            ),
                            factory.createExpressionStatement(factory.createYieldExpression(
                                undefined,
                                factory.createIdentifier("entity"),
                            )),
                        ], true),
                    ),
                ], deserializationErrorName)], true),
            ),
        ], true));
    }

    function* emitValidateStatements(
        mediaTypeObject: OpenAPIV3.MediaTypeObject,
        mediaTypePointerParts: string[],
        errorTypeName: string,
    ) {
        const schemaPointerParts = resolvePointerParts(
            [...mediaTypePointerParts, "schema"],
            mediaTypeObject.schema,
        );
        const schemaReference = schemaPointerParts && toReference(schemaPointerParts);
        const modelTypeName = schemaReference && nameIndex[schemaReference];

        assert(modelTypeName != null);

        const validateFn = stringifyIdentifier([
            "validate",
            modelTypeName,
        ]);

        yield factory.createVariableStatement(
            undefined,
            factory.createVariableDeclarationList([factory.createVariableDeclaration(
                factory.createIdentifier("invalidPaths"),
                undefined,
                undefined,
                factory.createArrayLiteralExpression([
                    factory.createSpreadElement(factory.createCallExpression(
                        factory.createPropertyAccessExpression(
                            factory.createIdentifier("shared"),
                            factory.createIdentifier(validateFn),
                        ),
                        undefined,
                        [factory.createIdentifier("entity")],
                    )),
                ], false),
            )], ts.NodeFlags.Const),
        );
        yield factory.createIfStatement(
            factory.createBinaryExpression(
                factory.createPropertyAccessExpression(
                    factory.createIdentifier("invalidPaths"),
                    factory.createIdentifier("length"),
                ),
                factory.createToken(ts.SyntaxKind.GreaterThanToken),
                factory.createNumericLiteral(0),
            ),
            factory.createBlock([
                factory.createThrowStatement(factory.createNewExpression(
                    factory.createPropertyAccessExpression(
                        factory.createIdentifier("lib"),
                        factory.createIdentifier(errorTypeName),
                    ),
                    undefined,
                    [factory.createIdentifier("invalidPaths")],
                )),
            ], true),
        );
    }

    function wrapTryCatchStatements(
        statements: ts.Statement[],
        errorTypeName: string,
    ) {
        return factory.createTryStatement(
            factory.createBlock(statements, true),
            factory.createCatchClause(
                factory.createVariableDeclaration(factory.createIdentifier("error")),
                factory.createBlock([
                    factory.createIfStatement(
                        factory.createBinaryExpression(
                            factory.createIdentifier("error"),
                            factory.createToken(ts.SyntaxKind.InstanceOfKeyword),
                            factory.createPropertyAccessExpression(
                                factory.createIdentifier("lib"),
                                factory.createIdentifier("DeserializationError"),
                            ),
                        ),
                        factory.createBlock([
                            factory.createThrowStatement(factory.createNewExpression(
                                factory.createPropertyAccessExpression(
                                    factory.createIdentifier("lib"),
                                    factory.createIdentifier(errorTypeName),
                                ),
                                undefined,
                                [],
                            )),
                        ], true),
                    ),
                    factory.createThrowStatement(factory.createIdentifier("error")),
                ], true),
            ),
            undefined,
        );
    }

}
