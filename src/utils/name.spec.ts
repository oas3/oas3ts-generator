import test from "tape-promise/tape.js";
import { stringifyIdentifier, stringifyType } from "./name.js";

test("name", async t => {
    t.equal(
        stringifyIdentifier(["aa+bb=cc"]),
        "aaBbCc",
    );
    t.equal(
        stringifyType(["aa+bb=cc"]),
        "AaBbCc",
    );
});
